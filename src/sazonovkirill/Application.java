package sazonovkirill;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class Application {
    public static void main(String[] args) throws IOException {
        while (true) {
            Path rootPath = Paths.get(args[0]);
            Files.walk(rootPath)
                    .filter(Files::isRegularFile)
                    .forEach((Path p) -> {
                        try {
                            String mimeType = Files.probeContentType(p);
                            if (mimeType != null && mimeType.contains("text")) {
                                List<String> lines = Files.readAllLines(p);
                                Collections.sort(lines);
                                for (String line : lines) {
                                    if (line.trim().length() > 0) {
                                        System.out.println(line.trim());
                                        break;
                                    }
                                }
                            }
                        } catch (IOException e) {
                            System.out.println(e);
                        }
                    });
        }
    }
}
